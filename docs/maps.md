---
marp: true
title: Marp CLI example
description: Hosting Marp slide deck on the web
theme: uncover
class: invert
# paginate: true
# _paginate: false

# Docs
# use fn button to insert emojis!

---
# **In depth review for web based mapping interface**

---
## **Reference sites**

#### 😎 OSM data from [openseamaps](https://www.openseamap.org/index.php?id=openseamap&no_cache=1)


---
## map frameworks



---
## Vector tiles



---
## Raster tiles


---
## 


